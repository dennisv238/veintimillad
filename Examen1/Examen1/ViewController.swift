//
//  ViewController.swift
//  Examen1
//
//  Created by Dennis Veintimilla on 5/6/18.
//  Copyright © 2018 Dennis Veintimilla. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var viewTitleLable: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBAction func nextButton(_ sender: Any) {
    
        performSegue(withIdentifier: "pasarNombre", sender: self)
    }
    
    var newUrl = ""
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pasarNombre" {
            let destino = segue.destination as? ViewController2
            destino?.nombre = nameTextField.text!
        }
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let urlString = "https://api.myjson.com/bins/72936"
        
        
        let url = URL(string: urlString)
        
        
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url!) { (data, response, error) in
            guard let data = data else {       //con el guard se asegura de que exista data, si no existe termina la funcion y sale de la ejecucion
                print("Error No data")
                return
            }
            guard let dataInfo = try? JSONDecoder().decode(ExamenDataInfo.self, from: data)
                else {
                    print("Error decoding Info")
                    return
            }
            
            DispatchQueue.main.async {
                
                self.viewTitleLable.text = "\(dataInfo.viewTitle)"
                self.dateLabel.text = "\(dataInfo.date)"
                
                let nextUrlString = "\(dataInfo.nextLink)"
                let nextUrl = URL(string: nextUrlString)
            }
        }
        
        
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

