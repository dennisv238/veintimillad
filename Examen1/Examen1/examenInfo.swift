//
//  examenInfo.swift
//  Examen1
//
//  Created by Dennis Veintimilla on 5/6/18.
//  Copyright © 2018 Dennis Veintimilla. All rights reserved.
//

import Foundation

struct ExamenDataInfo: Decodable {
    let viewTitle: String
    let date: String
    let nextLink: String
    
    //let mainWeather: String
    /*
     enum CodingKeys: String, CodingKey {
     case weather
     case mainWeather = "main_weather"
     */
}


