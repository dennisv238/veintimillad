//
//  ViewController2.swift
//  Examen1
//
//  Created by Dennis Veintimilla on 5/6/18.
//  Copyright © 2018 Dennis Veintimilla. All rights reserved.
//

import UIKit

class ViewController2: UIViewController, UITableViewDelegate, UITableViewDataSource{

    
    var nombre: String = ""
    var arrayLabels: [String] = []
    var arrayValues: [Int] = []
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var promedioLabel: UITextField!
    
    @IBOutlet weak var itemsTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel?.text = "Hi. " + nombre
        
        let urlString2 = "https://api.myjson.com/bins/182sje"
        
        let url2 = URL(string: urlString2)
        
        let session2 = URLSession.shared
        
        let task2 = session2.dataTask(with: url2!) { (data, response, error) in
            guard let data2 = data else {       //con el guard se asegura de que exista data, si no existe termina la funcion y sale de la ejecucion
                print("Error No data")
                return
            }
            
            guard let dataInfo = try? JSONDecoder().decode(padreApi.self, from: data2)
                else {
                    print("Error decoding Info")
                    return
            }
            
            DispatchQueue.main.async {
                
               self.arrayLabels.append("\(dataInfo.data[0].label)")
               self.arrayLabels.append("\(dataInfo.data[1].label)")
               self.arrayLabels.append("\(dataInfo.data[2].label)")
               self.arrayLabels.append("\(dataInfo.data[3].label)")
                
                self.arrayValues.append(dataInfo.data[0].value)
                self.arrayValues.append(dataInfo.data[1].value)
                self.arrayValues.append(dataInfo.data[2].value)
                self.arrayValues.append(dataInfo.data[3].value)
                
                
                //Promedio
                
                self.promedioLabel.text = "\((dataInfo.data[0].value + dataInfo.data[1].value + dataInfo.data[2].value + dataInfo.data[3].value) / 4)"
                
                //
                
                self.itemsTableView.reloadData()
            }

        // Do any additional setup after loading the view.
    }
        task2.resume()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        itemsTableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayLabels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as!
        TableViewCell
        
        cell.labelLabel.text = arrayLabels[indexPath.row]
        cell.valueLabel.text = "\(arrayValues[indexPath.row])"
        return cell
    }
    
    
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
