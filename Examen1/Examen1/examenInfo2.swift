//
//  examenInfo2.swift
//  Examen1
//
//  Created by Dennis Veintimilla on 6/6/18.
//  Copyright © 2018 Dennis Veintimilla. All rights reserved.
//

import Foundation

struct ExamenDataInfo2: Decodable {
    let viewTitle: String
    let date: String
    
}
   
    struct padreApi: Decodable {
        let data: [VectorData]
        
    }
    struct VectorData: Decodable {
        let label: String
        let value: Int
        
    }

